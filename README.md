Stworzymy aplikację która będzie imitować pracę Hotelu.
Do tego będą nam potrzebne następujące klasy:

Hotel (na którym będziemy wykonywać wszystkie czynności)
HotelEmployee (pracownik, który będzie przyjmować nasze zgłoszenie/będzie nas obsługiwać)
HotelCash (kasa, która generuje rachunki)
HotelReceipt (rachunek - widnieje na nim data i godzina rejestracji oraz wyrejestrowania, a także klasa pokoju i dane gościa)
HotelPhone (telefon, który pozwala na dzwonienie z hotelu, oraz posiada funkcję szybkiego wybierania)
HotelGuest (klasa gościa hotelowego)
PhoneCall (klasa reprezentująca połączenie telefoniczne)
SpeedDial (enum reprezentujący guziki speedial na telefonie)
HotelRoom (pokój hotelowy)
HotelPhone - klasa która posiada rejestr połączeń oraz funkcję dzwonienia. 
HotelPhone posiada listę połączeń, oraz metody wybierania. 

Telefon ma metodę "call" która jako parametr przyjmuje HotelGuest i numer telefonu (String). Poza tymi metodami, posiada metodę speedCall która jako parametr przyjmuje SpeedDial. Kolejna metoda telefonu to setSpeedDial która pozwala ustawić na dany speeddial wybrany numer telefonu. Metoda przyjmuje enuma SpeedDial oraz String (numer telefonu) który ma przypisać. HotelPhone rejestruje wszystkie połączenia call, ale nie połączenia speedial. Numery speeddial (które można ustawić) mogą być tylko 4 cyfrowe (numery wewnętrzne). 

Hotel ma jeden telefon, który tworzy się tuż przy utworzeniu hotelu (w konstruktorze). W hotelu istnieje pewna pula pracowników (lista). Hotel posiada metodę handleClient(HotelGuest) która dla każdego klienta iteruje po kolekcji klientów i przydziela kolejnego pracownika. Jeśli pula została przekroczona, przydzielany jest od nowa pracownik nr. 0. (trzeba zrobić licznik w klasie, który przy każdym handle się zwiększa, a po przekroczeniu rozmiaru używamy modulo).

Hotel posiada również rejestr pokoi. W konstruktorze stwórz listę pokoi. Każdy klient przychodząc LOSUJE MU SIĘ TYP SPRAWY. czyli losowo przydzielamy klientów do:
 A. Rejestracji (40%) - sprawdzamy czy pokój (losowy) wymagany przez klienta jest wolny, jeśli jest wolny to go dla niego zajmujemy.
 B. Wyrejestrowania (40%) - zwalniamy pokój klienta, generujemy mu rachunek za pokój (są 3 typy rachunków, LOW, STANDARD, HIGH, w zależności od okupowanego pokoju) .Rachunek generuje kasa.
 C. Dzwonienia (15%) - klient wykonuje połączenie zewnętrzne (call)
 D. Zgłaszania zażalenia (5%) - wykonujemy połączenia wewnętrzne (losowe) speediale.
Hotel posiada 10 pokoi (dodaj je w konstruktorze).

Main powinien pozwalac na wywołanie komendy: 
random (która wygeneruje zdarzenie, jedno z podanych wyżej 4)
guest id_goscia register nr_pokoju
guest id_goscia unregister nr_pokoju
call internal number
call external number

check calls - wylistowuje polaczenia
check rooms - wylistowuje dostepnosc pokoi

Kasa powinna tworzyć tickety jako fabryka abstrakcyjna.
